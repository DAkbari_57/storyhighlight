package com.tejappstudio.highlight.story.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tejappstudio.highlight.story.Activity.SplashActivity;
import com.tejappstudio.highlight.story.AppPref;
import com.tejappstudio.highlight.story.Loader.ImageLoader;
import com.tejappstudio.highlight.story.Model.FrameModel;
import com.tejappstudio.highlight.story.R;
import com.tejappstudio.highlight.story.helper.Stickers.StickerCatStatic_list;

import java.util.ArrayList;
import java.util.List;

public class StickerAdapter extends RecyclerView.Adapter<StickerAdapter.ViewHolder> {


    List<FrameModel> dataArrayList = new ArrayList<>();
    LayoutInflater layoutInflater;
    Context ctx;
    CustomItemClickListener clickListener;
    ImageLoader imageLoader;
    int cat_id,index,pos;
AppPref appPref;

    public StickerAdapter(Context ctx, int pos, CustomItemClickListener clickListener) {
        this.ctx = ctx;
        this.clickListener = clickListener;
        this.pos = pos;
        layoutInflater = layoutInflater.from(this.ctx);
        appPref = new AppPref(ctx);

        for (int i = 0; i < SplashActivity.sticker_temp.size(); i++) {
            if (SplashActivity.sticker_temp.get(i).cat == Integer.parseInt(StickerCatStatic_list.StickerCatlist.get(pos).getCatId()))
                this.index = i;
        }

        imageLoader = new ImageLoader(this.ctx);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.sticker_selector, viewGroup, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(v, viewHolder.getPosition(), index);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // viewHolder.imageView.setImageResource(dataArrayList.get(i).img);
//        Glide.with(ctx)
//                .load(ListBackSticker.sticker.get(position).getStick())
//                .override(500,500).into(viewHolder.imageView);

        if (Integer.parseInt(StickerCatStatic_list.StickerCatlist.get(pos).getCatPrem()) == 1 && !appPref.getString(AppPref.Purchase).equals("true")) {
            viewHolder.stick_lock.setVisibility(View.VISIBLE);
        } else {
            viewHolder.stick_lock.setVisibility(View.GONE);
        }

        Glide.with(ctx)
                .load(SplashActivity.sticker_temp.get(index).sticker_list.get(position))
                .into(viewHolder.imageView);


        /*

        Picasso.with(ctx)
                .load(DataStaticSticker.stickList.get(position).getImgUrl())
                .into(viewHolder.imageView);
*/

    }

    @Override
    public int getItemCount() {

        return SplashActivity.sticker_temp.get(index).sticker_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        ImageView stick_lock;

        public ViewHolder(View itemView) {
            super(itemView);

            this.imageView = itemView.findViewById(R.id.sticker_img);
            this.stick_lock = itemView.findViewById(R.id.stick_lock);

        }
    }
}
