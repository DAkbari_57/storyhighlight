package com.tejappstudio.highlight.story.helper.Background;

public class BackCat_list {

    public String catId;
    public String catName;
    public String catImg;
    public String catPrem;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatImg() {
        return catImg;
    }

    public void setCatImg(String catImg) {
        this.catImg = catImg;
    }

    public String getCatPrem() {
        return catPrem;
    }

    public void setCatPrem(String catPrem) {
        this.catPrem = catPrem;
    }
}
