package com.tejappstudio.highlight.story.adapter;

import android.view.View;

/**
 * Created by Shri Hari on 02-04-2018.
 */

public interface CustomClickListener {
    public void onItemClick(View v, int position, int pos1, int pos2,String str,int prem);
}