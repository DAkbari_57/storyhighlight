package com.tejappstudio.highlight.story.helper.Stickers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StickList {

    @SerializedName("img_id")
    @Expose
    private String imgId;
    @SerializedName("img_url")
    @Expose
    private String imgUrl;
    @SerializedName("cat_id")
    @Expose
    private String catId;

    public String getImgId() {
        return imgId;
    }

    public void setImgId(String imgId) {
        this.imgId = imgId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

}