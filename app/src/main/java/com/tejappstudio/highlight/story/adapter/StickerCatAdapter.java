package com.tejappstudio.highlight.story.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tejappstudio.highlight.story.Loader.ImageLoader;
import com.tejappstudio.highlight.story.Model.FrameModel;
import com.tejappstudio.highlight.story.R;
import com.tejappstudio.highlight.story.helper.Stickers.StickerCatStatic_list;

import java.util.ArrayList;
import java.util.List;

public class StickerCatAdapter extends RecyclerView.Adapter<StickerCatAdapter.ViewHolder>{

    List<FrameModel> dataArrayList = new ArrayList<>();
    LayoutInflater layoutInflater;
    Context ctx;
    CustomCatItemClickListener clickListener;
    ImageLoader imageLoader;



    public StickerCatAdapter(Context ctx,  CustomCatItemClickListener clickListener) {
        this.ctx = ctx;
        this.clickListener = clickListener;

        layoutInflater = layoutInflater.from(this.ctx);

        imageLoader = new ImageLoader(this.ctx);
    }

    @Override
    public StickerCatAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.backcat_item, viewGroup, false);
        final StickerCatAdapter.ViewHolder viewHolder = new StickerCatAdapter.ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(v, viewHolder.getPosition());

//                viewHolder.backCat.setBackgroundResource(R.color.white);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(StickerCatAdapter.ViewHolder viewHolder, final int position) {

        viewHolder.backCat_text.setText(StickerCatStatic_list.StickerCatlist.get(position).catName);

    }

    @Override
    public int getItemCount() {
        return StickerCatStatic_list.StickerCatlist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout backCat;
        TextView backCat_text;;

        public ViewHolder(View itemView) {
            super(itemView);

            backCat = itemView.findViewById(R.id.backCat);
            backCat_text = itemView.findViewById(R.id.backCat_text);

        }
    }
    
}
