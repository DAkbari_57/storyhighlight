package com.tejappstudio.highlight.story.helper.Background;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BackCatList {

    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("cat_name")
    @Expose
    private String catName;
    @SerializedName("cat_img")
    @Expose
    private String catImg;
    @SerializedName("cat_prem")
    @Expose
    private String catPrem;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatImg() {
        return catImg;
    }

    public void setCatImg(String catImg) {
        this.catImg = catImg;
    }

    public String getCatPrem() {
        return catPrem;
    }

    public void setCatPrem(String catPrem) {
        this.catPrem = catPrem;
    }

}