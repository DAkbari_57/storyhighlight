package com.tejappstudio.highlight.story.helper.Stickers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataStickerCat {
    @SerializedName("StickCat_List")
    @Expose
    private List<StickCatList> stickCatList = null;

    public List<StickCatList> getStickCatList() {
        return stickCatList;
    }

    public void setStickCatList(List<StickCatList> stickCatList) {
        this.stickCatList = stickCatList;
    }
}
