package com.tejappstudio.highlight.story.helper.Stickers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataSticker {
    @SerializedName("Stick_List")
    @Expose
    private List<StickList> stickList = null;

    public List<StickList> getStickList() {
        return stickList;
    }

    public void setStickList(List<StickList> stickList) {
        this.stickList = stickList;
    }
}
