package com.tejappstudio.highlight.story.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tejappstudio.highlight.story.R;
import com.tejappstudio.highlight.story.helper.Background.BackCatStatic_list;

public class BackCatAdapter extends RecyclerView.Adapter<BackCatAdapter.ViewHolder>{


    Context context;
    ImageView background_img;
    RecyclerView backCat_recyclr;
    LayoutInflater layoutInflater;
    CustomCatItemClickListener clickListener;



    public BackCatAdapter(Context context, CustomCatItemClickListener clickListener) {
        this.context = context;
        this.clickListener = clickListener;
//        this.background_img = background_img;
//        this.backCat_recyclr = backCat_recyclr;
        layoutInflater = layoutInflater.from(this.context);
    }

    @Override
    public BackCatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = layoutInflater.inflate(R.layout.backcat_item, parent, false);
        final BackCatAdapter.ViewHolder viewHolder = new BackCatAdapter.ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(v, viewHolder.getPosition());

//                viewHolder.backCat.setBackgroundResource(R.color.white);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final BackCatAdapter.ViewHolder holder, final int position) {

        holder.backCat_text.setText(BackCatStatic_list.BackCatlist.get(position).getCatName());
/*
        holder.backCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
                gridLayoutManager.setOrientation(GridLayoutManager.HORIZONTAL);
                backCat_recyclr.setLayoutManager(gridLayoutManager);
                backCat_recyclr.setAdapter(new BackAdapter(context, background_img, position));

//                holder.backCat.setBackgroundResource(R.color.white);

            }
        });
        */

    }

    @Override
    public int getItemCount() {
        return BackCatStatic_list.BackCatlist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout backCat;
        TextView backCat_text;

        //            ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            backCat = itemView.findViewById(R.id.backCat);
            backCat_text = itemView.findViewById(R.id.backCat_text);

        }

    }
    
}
