package com.tejappstudio.highlight.story.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tejappstudio.highlight.story.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.ViewHolder> {

    ArrayList<Integer> colors;
    LayoutInflater layoutInflater;
    Context ctx;
    CustomCatItemClickListener clickListener;

    public ColorAdapter(Context ctx, ArrayList<Integer> colors, CustomCatItemClickListener clickListener) {
        this.ctx = ctx;
        this.colors = colors;
        this.clickListener = clickListener;

        layoutInflater = layoutInflater.from(ctx);

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.color_item, viewGroup, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(v, viewHolder.getPosition());
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // viewHolder.imageView.setImageResource(dataArrayList.get(i).img);

//        viewHolder.bg_img.setColorFilter(colors.get(position));

//        viewHolder.bg_img.setColorFilter(R.color.brown);

//        viewHolder.bg_img.setColorFilter(ContextCompat.getColor(ctx,R.color.white));

//        Log.e("check color ", String.valueOf(ContextCompat.getColor(ctx,R.color.white)));

        viewHolder.bg_img.getBackground().setColorFilter(colors.get(position), PorterDuff.Mode.MULTIPLY );
    }

    @Override
    public int getItemCount() {
        return colors.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView bg_img;

        public ViewHolder(View itemView) {
            super(itemView);

            bg_img = itemView.findViewById(R.id.bg_img);

        }
    }


}

