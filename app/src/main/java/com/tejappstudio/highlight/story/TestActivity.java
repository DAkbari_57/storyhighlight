package com.tejappstudio.highlight.story;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import de.hdodenhof.circleimageview.CircleImageView;

public class TestActivity extends AppCompatActivity {

CircleImageView tmp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        @SuppressLint("ResourceType") String str = getResources().getString(R.color.black);

        Log.e("color check ",str);

        tmp = findViewById(R.id.tmp);

        tmp.setColorFilter(Color.parseColor(str));

    }


}
