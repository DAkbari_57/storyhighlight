package com.tejappstudio.highlight.story.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tejappstudio.highlight.story.Activity.SplashActivity;
import com.tejappstudio.highlight.story.AppPref;
import com.tejappstudio.highlight.story.Loader.ImageLoader;
import com.tejappstudio.highlight.story.R;
import com.tejappstudio.highlight.story.helper.Background.BackCatStatic_list;

import de.hdodenhof.circleimageview.CircleImageView;

public class BackAdapter extends RecyclerView.Adapter<BackAdapter.ViewHolder> {


    Context context;
    //    DataBack data;
    ImageView background_img;
    //    String catId;
    ImageLoader imageLoader;
    CustomItemClickListener clickListener;
    int pos, start, i, index, prem, cat_id;
    LayoutInflater layoutInflater;
    AppPref appPref;


    public BackAdapter(Context context, int pos, CustomItemClickListener clickListener) {
//        Log.e("hello adapter ", "reach adapter");
//        for (int i = 0; i < data.getBackList().size(); i++) {
//            Log.e("data url : ", data.getBackList().get(i).getImgUrl());
//        }
        this.context = context;
        this.pos = pos;
        this.clickListener = clickListener;
        layoutInflater = layoutInflater.from(this.context);
        appPref = new AppPref(context);

//        Log.e("back cat id is ", String.valueOf(pos));

//        this.data = data;
//        this.background_img = background_img;
////        this.catId = catId;
//        this.pos = pos;
        for (int i = 0; i < SplashActivity.temp.size(); i++) {
            if (SplashActivity.temp.get(i).cat == Integer.parseInt(BackCatStatic_list.BackCatlist.get(pos).getCatId()))
                this.index = i;
        }

        imageLoader = new ImageLoader(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = layoutInflater.inflate(R.layout.back_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(v, viewHolder.getPosition(), index);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if (Integer.parseInt(BackCatStatic_list.BackCatlist.get(pos).getCatPrem()) == 1 && !appPref.getString(AppPref.Purchase).equals("true")) {
            holder.back_lock.setVisibility(View.VISIBLE);
        } else {
            holder.back_lock.setVisibility(View.GONE);
        }

        String s = SplashActivity.temp.get(index).background_list.get(position);
        imageLoader.DisplayImage(s.substring(0, s.length() - 4) + "_thumb.jpg", R.color.background, holder.image);

//        Glide.with(context)
//                .load(DataStatic.backList.get(position).getImgUrl())
//                .override(500,500).into(holder.image);

/*
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                        imageLoader.DisplayImage(SplashActivity.temp.get(id-3).background_list.get(position),R.color.background,background_img);


//                        Picasso.get()
//                                .load(SplashActivity.temp.get(id-3).background_list.get(position))
//                                .into(background_img);

                Utils.showPleaseWait(context);

                Glide.with(context)
                        .load(SplashActivity.temp.get(index).background_list.get(position))
                        .override(500, 500).into(background_img);

                Utils.dismissPleaseWait();

            }
        });*/


        /*

        for (i = start+1; i < BackStatic_list.Backlist.size(); i++) {

            if (Integer.parseInt(BackStatic_list.Backlist.get(i).getImgCat()) == id) {

                imageLoader.DisplayImage(BackStatic_list.Backlist.get(i).getImgUrl(),R.color.background,holder.image);
    */
/*
        Glide.with(context)
                .load(DataStatic.backList.get(position).getImgUrl())
                .override(500,500).into(holder.image);
*//*


                holder.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        imageLoader.DisplayImage(BackStatic_list.Backlist.get(i).getImgUrl(),R.color.background,background_img);
 */
/*               Glide.with(context)
                        .load(DataStatic.backList.get(position).getImgUrl())
                        .override(500,500).into(background_img);
                        *//*

                    }
                });

                start = i;
                break;

            } else {
                Log.e("in else ", "");
            }
        }

*/

    }

    @Override
    public int getItemCount() {
        return SplashActivity.temp.get(index).background_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView image;
        ImageView back_lock;

        //            ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.image = itemView.findViewById(R.id.back_img);
            this.back_lock = itemView.findViewById(R.id.back_lock);

        }

    }

}
