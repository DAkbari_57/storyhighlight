package com.tejappstudio.highlight.story.helper.Background;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataBack{

    @SerializedName("Back_List")
    @Expose
    public List<BackList> backList = null;

    public List<BackList> getBackList() {
        return backList;
    }

    public void setBackList(List<BackList> backList) {
        this.backList = backList;
    }

}
