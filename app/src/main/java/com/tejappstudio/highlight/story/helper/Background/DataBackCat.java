package com.tejappstudio.highlight.story.helper.Background;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataBackCat {
    @SerializedName("BackCat_List")
    @Expose
    private List<BackCatList> backCatList = null;

    public List<BackCatList> getBackCatList() {
        return backCatList;
    }

    public void setBackCatList(List<BackCatList> backCatList) {
        this.backCatList = backCatList;
    }
}
