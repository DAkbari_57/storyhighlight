package com.tejappstudio.highlight.story;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;


public class Utils {

    public static String getIMEI(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getDeviceId();
    }

    public static boolean isConnected(Context activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public static void removeInputMethod(View view){
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private static ProgressDialog pleaseWait;

    public static void showPleaseWait(Context context) {
        pleaseWait = new ProgressDialog(context);
        pleaseWait.setMessage("Please Wait...");
        pleaseWait.setCancelable(false);
        pleaseWait.show();
    }

    public static void dismissPleaseWait() {
        if (pleaseWait.isShowing()) {
            pleaseWait.dismiss();
        }
    }

}
