package com.tejappstudio.highlight.story;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class AppPref {


    public static final String USER_PREFS = "USER_PREFS";
    public static SharedPreferences appSharedPref;
    public static SharedPreferences.Editor prefEditor;

    @SuppressLint("CommitPrefEdits")
    public AppPref(Context context) {
        appSharedPref = context.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
        prefEditor = appSharedPref.edit();
    }

    //Set as Boolean
    public static String Purchase = "Purchase";

    public static String HomeRestList = "homeRestList";


    public static String getString(String key) {
        return appSharedPref.getString(key, " ");
    }

    public void setString(String key, String value) {

        prefEditor.putString(key, value).apply();
    }

    public static boolean getBoolean(String key) {
        return appSharedPref.getBoolean(key, false);
    }

    public void setBoolean(String key, boolean value) {
        prefEditor.putBoolean(key, value).apply();
    }


    public void clearPref() {
        setString(Purchase, "");
    }
}
