package com.tejappstudio.highlight.story.helper.Background;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BackList {

    @SerializedName("img_id")
    @Expose
    private String imgId;
    @SerializedName("img_url")
    @Expose
    private String imgUrl;
    @SerializedName("img_cat")
    @Expose
    private String imgCat;

    public String getImgId() {
        return imgId;
    }

    public void setImgId(String imgId) {
        this.imgId = imgId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getImgCat() {
        return imgCat;
    }

    public void setImgCat(String imgCat) {
        this.imgCat = imgCat;
    }

}
