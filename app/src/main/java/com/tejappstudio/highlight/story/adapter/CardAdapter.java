package com.tejappstudio.highlight.story.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tejappstudio.highlight.story.Activity.SplashActivity;
import com.tejappstudio.highlight.story.AppPref;
import com.tejappstudio.highlight.story.Loader.ImageLoader;
import com.tejappstudio.highlight.story.R;
import com.tejappstudio.highlight.story.Utils;
import com.tejappstudio.highlight.story.helper.Background.BackCatStatic_list;
import com.tejappstudio.highlight.story.helper.Background.Back_list;
import com.tejappstudio.highlight.story.helper.Stickers.StickerStatic_list;
import com.tejappstudio.highlight.story.helper.Stickers.Sticker_list;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.viewholder> {

    Context context;
    List<Back_list> back_list;
    List<Sticker_list> stick_list;
    LayoutInflater layoutInflater;
    CustomClickListener clickListener;
    ImageLoader imageLoader;
    ArrayList<String> bg;
    Random random;
    String str;
    int cat_id, pos, start, index;
    AppPref appPref;

    public CardAdapter(Context context, int pos, List<Back_list> back_list, List<Sticker_list> stick_list, CustomClickListener clickListener) {
//        Log.e("back static size ", String.valueOf(BackStatic_list.Backlist.size()));

        this.context = context;
        this.back_list = back_list;
        this.stick_list = stick_list;
        this.clickListener = clickListener;
        this.pos = pos;
        this.cat_id = Integer.parseInt(BackCatStatic_list.BackCatlist.get(pos).getCatId());
        layoutInflater = layoutInflater.from(context);
        appPref = new AppPref(context);

        for (int i = 0; i < SplashActivity.temp.size(); i++) {
            if (SplashActivity.temp.get(i).cat == cat_id)
                this.index = i;
        }


        imageLoader = new ImageLoader(context);
        random = new Random();

        bg = new ArrayList<>();
        bg.add("sbg_1");
        bg.add("sbg_2");
    }

    @Override
    public CardAdapter.viewholder onCreateViewHolder(ViewGroup parent, int i) {
        View view = layoutInflater.inflate(R.layout.image_item, parent, false);
        final CardAdapter.viewholder viewHolder = new CardAdapter.viewholder(view);
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                clickListener.onItemClick(v ,viewHolder.get);
//            }
//        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CardAdapter.viewholder holder, final int position) {

        if (Integer.parseInt(BackCatStatic_list.BackCatlist.get(pos).getCatPrem()) == 1 && !appPref.getString(AppPref.Purchase).equals("true")) {
            holder.lock.setVisibility(View.VISIBLE);
        } else {
            holder.lock.setVisibility(View.GONE);
        }


//        Glide.with(context)
//                .load("http://freeblockchaintutorials.com/tempp/highlights/imgs/1_thumb.jpg")
//                .into(holder.image);

        String s = SplashActivity.temp.get(index).background_list.get(position);
        imageLoader.DisplayImage(s.substring(0, s.length() - 4) + "_thumb.jpg", R.color.background, holder.image);

//        Glide.with(context)
//                .load(SplashActivity.temp.get(cat_id-3).background_list.get(position))
//                .into(holder.image);

//        Picasso.get()
//                .load(s.substring(0,s.length()-4) + "_thumb.jpg")
//                .into(holder.image);


//        imageLoader.DisplayImage(SplashActivity.temp.get(cat_id - 3).background_list.get(position), R.color.background, holder.image);


//        Log.e("sticker url is : ",StickerStatic_list.Stickerlist.get(i).getImgUrl());
        imageLoader.DisplayImage(StickerStatic_list.Stickerlist.get(position).getImgUrl(), R.color.white, holder.sticker);

        str = bg.get(random.nextInt(1 + 1));

//        Log.e("adapter sbg---------", str);

        Glide.with(context)
                .load(context.getResources().getIdentifier(str, "drawable", context.getPackageName()))
                .override(500, 500).into(holder.spot);

        holder.sticker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                        Log.e("Item Clicked ", "huuuuuuuuuuuuuuuuuuuuuuu");

                Utils.showPleaseWait(context);

//                Log.e("back id ", String.valueOf(position));
//                Log.e("sticker id ", StickerStatic_list.Stickerlist.get(position).getImgId());
//                Toast.makeText(context, "clicked", Toast.LENGTH_SHORT).show();
                clickListener.onItemClick(v, Integer.parseInt(StickerStatic_list.Stickerlist.get(position).getImgId()),
                        position, index, str, Integer.parseInt(BackCatStatic_list.BackCatlist.get(pos).getCatPrem()));
            }
        });

    }

    @Override
    public int getItemCount() {
//        return SplashActivity.size.get(cat_id - 3);
        if (SplashActivity.temp.get(index).background_list.size() > 10) return 10;
        return SplashActivity.temp.get(index).background_list.size();

//        for (int i = 0; i < SplashActivity.temp.size(); i++) {
//            if (SplashActivity.temp.get(i).cat == cat_id)
//                return SplashActivity.temp.get(i).background_list.size();
//        }
//        return 0;
    }

    public static class viewholder extends RecyclerView.ViewHolder {

        CircleImageView image;
        CircleImageView spot;
        ImageView sticker;
        ImageView lock;

        public viewholder(@NonNull View itemView) {
            super(itemView);

            this.image = itemView.findViewById(R.id.img);
            this.spot = itemView.findViewById(R.id.spot);
            this.sticker = itemView.findViewById(R.id.sticker);
            this.lock = itemView.findViewById(R.id.lock);

        }
    }


}