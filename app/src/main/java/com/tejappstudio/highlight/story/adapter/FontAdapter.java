package com.tejappstudio.highlight.story.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tejappstudio.highlight.story.R;

import java.util.ArrayList;

public class FontAdapter extends RecyclerView.Adapter<FontAdapter.ViewHolder> {


    ArrayList<String> fontname;
    LayoutInflater layoutInflater;
    Context ctx;
    CustomCatItemClickListener clickListener;

    public FontAdapter(Context ctx, ArrayList<String> fontname, CustomCatItemClickListener clickListener) {
        //   Log.e("font test","in font adapter");
        for (int i = 0; i < fontname.size(); i++) {
            //Log.e("font test",fontname.get(i));
        }
        this.ctx = ctx;
        this.clickListener = clickListener;
        this.fontname = fontname;

        layoutInflater = LayoutInflater.from(ctx);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView font;

        public ViewHolder(View itemView) {
            super(itemView);

            this.font = itemView.findViewById(R.id.font);

        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.font_layout, viewGroup, false);


        final ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(v, viewHolder.getPosition());
            }
        });
        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        Log.e("Current", "OnbBIndVIew HOLDER");

        viewHolder.font.setText("Insta Highlight");

//        font_txt.setTypeface(Typeface.MONOSPACE);

//        Typeface tf = Typeface.createFromAsset(ctx.getAssets(),"fonts/b.ttf");
//        viewHolder.font_txt.setTypeface(tf);

//        viewHolder.font.setTypeface(Typeface.createFromAsset(ctx.getAssets(), "fonts/a.ttf"));

        viewHolder.font.setTypeface(Typeface.createFromAsset(ctx.getAssets(), "fonts/" + fontname.get(position)));
    }

    @Override
    public int getItemCount() {
        return fontname.size();
    }
}

