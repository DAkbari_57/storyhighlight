package com.tejappstudio.highlight.story.utils;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * extracting Typeface from Assets is a heavy operation,
 * we want to make sure that we cache the typefaces for reuse
 */
public class FontProvider {

    private static final String DEFAULT_FONT_NAME = "0";

    private final Map<String, Typeface> typefaces;
    private final Map<String, String> fontNameToTypefaceFile;
    private final Resources resources;
    private final List<String> fontNames;

    public FontProvider(Resources resources) {
        this.resources = resources;

        typefaces = new HashMap<>();

        // populate fonts
        fontNameToTypefaceFile = new HashMap<>();
        fontNameToTypefaceFile.put("0", "a.ttf");
        fontNameToTypefaceFile.put("1", "b.ttf");
        fontNameToTypefaceFile.put("2", "c.ttf");
        fontNameToTypefaceFile.put("3", "d.ttf");
        fontNameToTypefaceFile.put("4", "e.ttf");
        fontNameToTypefaceFile.put("5", "f.ttf");
        fontNameToTypefaceFile.put("6", "g.ttf");
        fontNameToTypefaceFile.put("7", "h.ttf");
        fontNameToTypefaceFile.put("8", "i.ttf");
        fontNameToTypefaceFile.put("9", "j.ttf");
        fontNameToTypefaceFile.put("10", "k.ttf");
        fontNameToTypefaceFile.put("11", "l.ttf");
        fontNameToTypefaceFile.put("12", "m.ttf");
        fontNameToTypefaceFile.put("13", "n.ttf");
        fontNameToTypefaceFile.put("14", "o.ttf");
        fontNameToTypefaceFile.put("15", "p.ttf");
        fontNameToTypefaceFile.put("16", "q.ttf");
        fontNameToTypefaceFile.put("17", "r.ttf");
        fontNameToTypefaceFile.put("18", "s.ttf");
        fontNameToTypefaceFile.put("19", "t.ttf");
        fontNameToTypefaceFile.put("20", "u.ttf");
        fontNameToTypefaceFile.put("21", "v.ttf");
        fontNameToTypefaceFile.put("22", "w.ttf");
        fontNameToTypefaceFile.put("23", "x.ttf");
        fontNameToTypefaceFile.put("24", "y.ttf");
        fontNameToTypefaceFile.put("25", "z.ttf");
        fontNameToTypefaceFile.put("26", "za.ttf");
        fontNameToTypefaceFile.put("27", "zb.ttf");
        fontNameToTypefaceFile.put("28", "zc.ttf");
        fontNameToTypefaceFile.put("29", "zd.ttf");
        fontNameToTypefaceFile.put("30", "ze.ttf");
        fontNameToTypefaceFile.put("31", "zf.ttf");
        fontNameToTypefaceFile.put("32", "zg.ttf");
        fontNameToTypefaceFile.put("33", "zh.ttf");
        fontNameToTypefaceFile.put("34", "zi.ttf");
        fontNameToTypefaceFile.put("35", "zj.ttf");
        fontNameToTypefaceFile.put("36", "zk.ttf");
        fontNameToTypefaceFile.put("37", "zl.ttf");
        fontNameToTypefaceFile.put("38", "zm.ttf");
        fontNameToTypefaceFile.put("39", "zn.ttf");
        fontNameToTypefaceFile.put("40", "zo.ttf");
        fontNameToTypefaceFile.put("41", "zp.ttf");
        fontNameToTypefaceFile.put("42", "zq.ttf");
        fontNameToTypefaceFile.put("43", "zr.ttf");
        fontNameToTypefaceFile.put("44", "zs.ttf");
        fontNameToTypefaceFile.put("45", "zt.ttf");
        fontNameToTypefaceFile.put("46", "zu.ttf");



        fontNames = new ArrayList<>(fontNameToTypefaceFile.keySet());
    }

    /**
     * @param typefaceName must be one of the font names provided from {@link FontProvider#getFontNames()}
     * @return the Typeface associated with {@code typefaceName}, or {@link Typeface#DEFAULT} otherwise
     */
    public Typeface getTypeface(@Nullable String typefaceName) {
        if (TextUtils.isEmpty(typefaceName)) {
            return Typeface.DEFAULT;
        } else {
            //noinspection Java8CollectionsApi
            if (typefaces.get(typefaceName) == null) {
                typefaces.put(typefaceName,
                        Typeface.createFromAsset(resources.getAssets(), "fonts/" + fontNameToTypefaceFile.get(typefaceName)));
            }
            return typefaces.get(typefaceName);
        }
    }

    /**
     * use {@link FontProvider#getTypeface(String) to get Typeface for the font name}
     *
     * @return list of available font names
     */
    public List<String> getFontNames() {
        return fontNames;
    }

    /**
     * @return Default Font Name - <b>Helvetica</b>
     */
    public String getDefaultFontName() {
        return DEFAULT_FONT_NAME;
    }
}