package com.tejappstudio.highlight.story.adapter;

import android.view.View;

/**
 * Created by Shri Hari on 02-04-2018.
 */

public interface CustomCatItemClickListener {
    public void onItemClick(View v, int position);
}