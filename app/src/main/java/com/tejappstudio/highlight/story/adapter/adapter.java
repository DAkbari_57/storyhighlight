package com.tejappstudio.highlight.story.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.tejappstudio.highlight.story.Activity.ImageditActivity;
import com.tejappstudio.highlight.story.AppPref;
import com.tejappstudio.highlight.story.Loader.ImageLoader;
import com.tejappstudio.highlight.story.R;
import com.tejappstudio.highlight.story.Utils;
import com.tejappstudio.highlight.story.helper.Background.BackCatStatic_list;
import com.tejappstudio.highlight.story.helper.Background.BackStatic_list;
import com.tejappstudio.highlight.story.helper.Background.Back_list;
import com.tejappstudio.highlight.story.helper.Stickers.StickerStatic_list;
import com.tejappstudio.highlight.story.helper.Stickers.Sticker_list;
import com.tejappstudio.highlight.story.util.util.IabHelper;
import com.tejappstudio.highlight.story.util.util.IabResult;
import com.tejappstudio.highlight.story.util.util.Inventory;
import com.tejappstudio.highlight.story.util.util.Purchase;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class adapter extends RecyclerView.Adapter<adapter.viewholder> {

    Context context;
    List<Back_list> back_list;
    List<Sticker_list> stick_list;
    LayoutInflater layoutInflater;
    CustomClickListener clickListener;
    RecyclerView recyclr;
    ImageLoader imageLoader;
    ArrayList<String> bg;
    Random random;
    String str;
    AppPref appPref;
    IabHelper mHelper;
    static String ITEM_SKU = "product1";

    public adapter(Context context, RecyclerView recyclr, List<Back_list> back_list, List<Sticker_list> stick_list, CustomClickListener clickListener) {
//        Log.e("in adpter ", "hellooooooooooooo");
        this.context = context;
//        Log.e("size of cat list ", String.valueOf(BackCatStatic_list.BackCatlist.size()));
        this.back_list = back_list;
        this.stick_list = stick_list;
        this.clickListener = clickListener;
        this.recyclr = recyclr;
        layoutInflater = layoutInflater.from(context);
        appPref = new AppPref(context);
//        Log.e("hello after contruct", "");

        String base64EncodedPublicKey =
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA10yFjSNqG2/VzMBCpe/7vtTz0QapXpPk7AnQu+/6HxQOjNPFEXir5FIbRC6E3hLqjZmDR/fzSvMvVV6UMUiF3oYO/J53kZoWDx9UXOVCjKp0r0v10/atUmZfTR69Q2IN/9IFLyDgIKl5YWpiOMwZgWF+rkruJm+ACF3ZwRsoYqdnUCKd42DQijuiS1RiY2A0fCfxm6nv3kMOO4kEu8SPtWFyIgVb5sVHDg0+RayAFoRqO4woGT2yABM/NO6/3k4+sv15mRsdJYlV5WV6tDCRe/KlvYqckPHmCbGF95HJ9uKXOFfVyQXycjbbBhP1viuaNsje9LNCfZhsaLortlYktQIDAQAB";

        mHelper = new IabHelper(this.context, base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

            public void onIabSetupFinished(IabResult result) {
                String TAG = "ImageEdit Activity";
                if (!result.isSuccess()) {
                    Log.d(TAG, "In-app Billing setup failed: " + result);
                } else {
                    Log.d(TAG, "In-app Billing is set up OK");
                }
            }
        });

//        imageLoader = new ImageLoader(context);
//        random = new Random();
//
//        bg = new ArrayList<>();
//        bg.add("sbg_1");
//        bg.add("sbg_2");
    }

    @Override
    public viewholder onCreateViewHolder(ViewGroup parent, int i) {
        View view = layoutInflater.inflate(R.layout.cardview_item, parent, false);
        final viewholder viewHolder = new viewholder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final viewholder holder, final int position) {

        holder.title.setText(BackCatStatic_list.BackCatlist.get(position).getCatName());

        GridLayoutManager gridLayoutManager = new GridLayoutManager(context.getApplicationContext(), 5, GridLayoutManager.VERTICAL, false);
        holder.recycler_card.setLayoutManager(gridLayoutManager);
        holder.recycler_card.setAdapter(new CardAdapter(context, position, BackStatic_list.Backlist, StickerStatic_list.Stickerlist, new CustomClickListener() {

            public void onItemClick(View v, int sticker_pos, int back_pos, int cat_id, String str, int prem) {

                if (prem == 1 && !appPref.getString(AppPref.Purchase).equals("true")) {

                    Utils.dismissPleaseWait();
                    final Dialog pur_dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    pur_dialog.setContentView(R.layout.purchase);
                    Button btn = pur_dialog.findViewById(R.id.purchase);
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            load_page();
                            try {
                                mHelper.launchPurchaseFlow((Activity) context, ITEM_SKU, 10001,
                                        mPurchaseFinishedListener, "mypurchasetoken");
                            } catch (IabHelper.IabAsyncInProgressException e) {
                                e.printStackTrace();
                            }
//                            appPref.setString(AppPref.Purchase,"true");
//                            Log.e("shared preference ",appPref.getString(AppPref.Purchase));
                            pur_dialog.dismiss();
                        }
                    });
                    pur_dialog.show();

                } else {
                    Intent imageintent = new Intent(context, ImageditActivity.class);
                    imageintent.putExtra("sticker_position", sticker_pos);
                    imageintent.putExtra("back_position", back_pos);
                    imageintent.putExtra("cat id", cat_id);
                    imageintent.putExtra("sbg", str);
                    context.startActivity(imageintent);
                }
            }

        }));


    }

    @Override
    public int getItemCount() {
        return BackCatStatic_list.BackCatlist.size();
//        return  0;
    }

    public static class viewholder extends RecyclerView.ViewHolder {

//        CircleImageView image;
//        CircleImageView spot;
//        ImageView sticker;

        TextView title;
        RecyclerView recycler_card;

        //            ImageView image;
        public viewholder(@NonNull View itemView) {
            super(itemView);

//            this.image = itemView.findViewById(R.id.img);
//            this.spot = itemView.findViewById(R.id.spot);
//            this.sticker = itemView.findViewById(R.id.sticker);

            this.title = itemView.findViewById(R.id.title);
            this.recycler_card = itemView.findViewById(R.id.recycler_card);

        }
    }

    public void load_page() {
//        Log.e("before adap", "hooooooooooo");
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 1);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclr.setLayoutManager(gridLayoutManager);
//        Log.e("mid adap", "huuuuuuuuuuuiiiiiii");
        recyclr.setAdapter(new adapter(context, recyclr, BackStatic_list.Backlist, StickerStatic_list.Stickerlist, new CustomClickListener() {

            public void onItemClick(View v, final int position, int pos1, int pos2, final String str, int pur) {

//                Log.e("maiin sbg---------", str);

            }
//
        }));
    }


    //******purchase methods


    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        @SuppressLint("LongLogTag")
        public void onIabPurchaseFinished(IabResult result,
                                          Purchase purchase) {
            if (result.isFailure()) {
                // Handle error
//                Log.e("IN APP PUR OnIabPurchaseFinishedListener", "Errorr" + result.getMessage());
                Toast.makeText(context, "Purchase Failed Try again please", Toast.LENGTH_LONG).show();
                return;


            } else if (purchase.getSku().equals(ITEM_SKU)) {
                consumeItem();

            }

        }
    };


    public void consumeItem() {
        try {
            mHelper.queryInventoryAsync(mReceivedInventoryListener);
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
        }
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {


            if (result.isFailure()) {
                Toast.makeText(context, "Purchase Failed Try again please", Toast.LENGTH_LONG).show();
                // Handle failure
            } else {
                try {
                    mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                            mConsumeFinishedListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase,
                                              IabResult result) {

                    if (result.isSuccess()) {
                        appPref.setString(AppPref.Purchase, "true");
//                        Log.e("shared preference ", appPref.getString(AppPref.Purchase));

                    } else {
                        // handle error
                        Toast.makeText(context, "Purchase Failed Try again please", Toast.LENGTH_LONG).show();
                    }
                }
            };


}
