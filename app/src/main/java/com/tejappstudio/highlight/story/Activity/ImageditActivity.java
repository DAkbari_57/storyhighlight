package com.tejappstudio.highlight.story.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;
import com.tejappstudio.highlight.story.AppPref;
import com.tejappstudio.highlight.story.BuildConfig;
import com.tejappstudio.highlight.story.Model.FrameModel;
import com.tejappstudio.highlight.story.R;
import com.tejappstudio.highlight.story.Utils;
import com.tejappstudio.highlight.story.adapter.BackAdapter;
import com.tejappstudio.highlight.story.adapter.BackCatAdapter;
import com.tejappstudio.highlight.story.adapter.ColorAdapter;
import com.tejappstudio.highlight.story.adapter.CustomCatItemClickListener;
import com.tejappstudio.highlight.story.adapter.CustomItemClickListener;
import com.tejappstudio.highlight.story.adapter.FontAdapter;
import com.tejappstudio.highlight.story.adapter.StickerAdapter;
import com.tejappstudio.highlight.story.adapter.StickerCatAdapter;
import com.tejappstudio.highlight.story.helper.Background.BackCatStatic_list;
import com.tejappstudio.highlight.story.helper.PreviewImage;
import com.tejappstudio.highlight.story.helper.Stickers.StickerCatStatic_list;
import com.tejappstudio.highlight.story.helper.Stickers.StickerStatic_list;
import com.tejappstudio.highlight.story.util.util.IabHelper;
import com.tejappstudio.highlight.story.util.util.IabResult;
import com.tejappstudio.highlight.story.util.util.Inventory;
import com.tejappstudio.highlight.story.util.util.Purchase;
import com.tejappstudio.highlight.story.utils.FontProvider;
import com.tejappstudio.highlight.story.viewmodel.Font;
import com.tejappstudio.highlight.story.viewmodel.Layer;
import com.tejappstudio.highlight.story.viewmodel.TextLayer;
import com.tejappstudio.highlight.story.widget.MotionView;
import com.tejappstudio.highlight.story.widget.TextEditorDialogFragment;
import com.tejappstudio.highlight.story.widget.entity.ImageEntity;
import com.tejappstudio.highlight.story.widget.entity.MotionEntity;
import com.tejappstudio.highlight.story.widget.entity.TextEntity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.next.slidebottompanel.SlideBottomPanel;


public class ImageditActivity extends AppCompatActivity implements TextEditorDialogFragment.OnTextLayerCallback {


    IabHelper mHelper;
    static String ITEM_SKU = "product1";


    private SlideBottomPanel slide;
    private RecyclerView back_recyclr, selector_recyclr;
    //    public CatData catData;
    LinearLayout linear_wallpaper, linear_text, linear_sticker;
    ImageView img_bg, img_sticker;
    //    StickerView stickerView;
//    StickerView stickerView;
    protected MotionView motionView;
    ImageView background_img, iv_back, iv_check;
    LinearLayout img_back, img_preview, img_download, linear_shown, linear_sticker_select, sticker_delete,
            sticker_back, linear_txt_back, linear_txt_font, linear_txt_delete, linear_txt_edit, linear_txt_color, linear_gallery;
    private int MY_REQUEST_CODE = 5;
    public static Bitmap finalEditedBitmapImage;
    private FrameLayout main_frame;
    ArrayList<Integer> stickerviewId = new ArrayList<>();
    boolean isAlreadySave = false, flag = true;
    public static String _url;
    ArrayList<FrameModel> sticker;
    ArrayList<String> fontname;
    File dir = null;
    LinearLayout linear_text_panel;
    public MotionEntity selectedEntity;
    private FontProvider fontProvider;
    TextLayer textLayer;
    List<String> fontname_key;
    ArrayList<Integer> colors;
    Bitmap image;
    int RE_GALLERY = 2;
    //        ImageLoader imageLoader;
    ImageLoader imageLoader;
    AppPref appPref;

    ImageView test_back, test_check, back, done;
    RecyclerView test_recyclr, backCat_recyclr, cat_recyclr;
    View myView, backView;
    boolean isUp, isBackUp;
    Dialog pur_dialog;

    InterstitialAd interstitialAd;


    private final MotionView.MotionViewCallback motionViewCallback = new MotionView.MotionViewCallback() {
        @Override
        public void onEntitySelected(@Nullable MotionEntity entity) {
            if (entity instanceof TextEntity) {
                img_preview.setVisibility(View.GONE);
                linear_shown.setVisibility(View.GONE);
                linear_text_panel.setVisibility(View.VISIBLE);
            } else {
                img_preview.setVisibility(View.VISIBLE);
                linear_text_panel.setVisibility(View.GONE);
                linear_shown.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onStickerEntitySelected(@Nullable MotionEntity entity) {
            if (entity instanceof ImageEntity) {
//                Log.e("in sticker select ", "sticker");
                linear_shown.setVisibility(View.GONE);
                img_preview.setVisibility(View.GONE);
                linear_sticker_select.setVisibility(View.VISIBLE);
                motionView.initEntityBorder(entity);
            } else {
                linear_sticker_select.setVisibility(View.GONE);
                linear_shown.setVisibility(View.VISIBLE);
                img_preview.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onEntityDoubleTap(@NonNull MotionEntity entity) {
            startTextEntityEditing();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagedit);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);



        String base64EncodedPublicKey =
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA10yFjSNqG2/VzMBCpe/7vtTz0QapXpPk7AnQu+/6HxQOjNPFEXir5FIbRC6E3hLqjZmDR/fzSvMvVV6UMUiF3oYO/J53kZoWDx9UXOVCjKp0r0v10/atUmZfTR69Q2IN/9IFLyDgIKl5YWpiOMwZgWF+rkruJm+ACF3ZwRsoYqdnUCKd42DQijuiS1RiY2A0fCfxm6nv3kMOO4kEu8SPtWFyIgVb5sVHDg0+RayAFoRqO4woGT2yABM/NO6/3k4+sv15mRsdJYlV5WV6tDCRe/KlvYqckPHmCbGF95HJ9uKXOFfVyQXycjbbBhP1viuaNsje9LNCfZhsaLortlYktQIDAQAB";

        mHelper = new IabHelper(this, base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

            public void onIabSetupFinished(IabResult result) {
                String TAG = "ImageEdit Activity";
                if (!result.isSuccess()) {
//                    Log.d(TAG, "In-app Billing setup failed: " + result);
                } else {
//                    Log.d(TAG, "In-app Billing is set up OK");
                }
            }
        });

        interstitialAd = new InterstitialAd(ImageditActivity.this, "319129505471336_319130505471236");
        if (SplashActivity.ad_count == 1 || SplashActivity.ad_count == 3 || SplashActivity.ad_count == 5 || SplashActivity.ad_count >= 7) {
            interstitialAd.loadAd();
        }
        SplashActivity.ad_count++;
        final String TAG = "Imagedit Intstitial ad";
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback


//                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                // Interstitial dismissed callback
//                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
//                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Interstitial ad is loaded and ready to be displayed
//                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                // Show the ad
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
//                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
//                 Ad impression logged callback
//                Log.d(TAG, "Interstitial ad impression logged!");
            }
        });


        appPref = new AppPref(ImageditActivity.this);


        slide = findViewById(R.id.sbv);
        back_recyclr = findViewById(R.id.back_recyclr);


        linear_wallpaper = findViewById(R.id.linear_wallpaper);
        linear_text = findViewById(R.id.linear_text);
        linear_sticker = findViewById(R.id.linear_sticker);

        background_img = findViewById(R.id.background_img);
        motionView = findViewById(R.id.main_motion_view);
        motionView.setMotionViewCallback(motionViewCallback);


        linear_shown = findViewById(R.id.linear_shown);
        linear_sticker_select = findViewById(R.id.linear_sticker_select);
        linear_text_panel = findViewById(R.id.linear_text_panel);
        sticker_delete = findViewById(R.id.sticker_delete);
        sticker_back = findViewById(R.id.sticker_back);

        img_back = findViewById(R.id.img_back);
        img_preview = findViewById(R.id.img_preview);
        img_download = findViewById(R.id.img_download);
        iv_back = findViewById(R.id.iv_back);
        iv_check = findViewById(R.id.iv_check);

        main_frame = findViewById(R.id.main_frame);
        linear_txt_back = findViewById(R.id.linear_txt_back);
        linear_txt_delete = findViewById(R.id.linear_txt_delete);
        linear_txt_font = findViewById(R.id.linear_txt_font);
        linear_txt_edit = findViewById(R.id.linear_txt_edit);
        linear_txt_color = findViewById(R.id.linear_txt_color);


        linear_gallery = findViewById(R.id.linear_gallery);


        myView = findViewById(R.id.my_view);
        backView = findViewById(R.id.backView);
        test_recyclr = findViewById(R.id.test_recyclr);
        backCat_recyclr = findViewById(R.id.backCat_recyclr);
        cat_recyclr = findViewById(R.id.cat_recyclr);
        test_back = findViewById(R.id.test_back);
        test_check = findViewById(R.id.test_check);
        back = findViewById(R.id.back);
        done = findViewById(R.id.done);

        myView.setVisibility(View.INVISIBLE);
        backView.setVisibility(View.INVISIBLE);
        isUp = false;
        isBackUp = false;


        this.fontProvider = new FontProvider(getResources());

        fontname_key = new ArrayList<>();
        fontname_key = fontProvider.getFontNames();


        String[] colorsTxt = getApplicationContext().getResources().getStringArray(R.array.colors);
        colors = new ArrayList<Integer>();
        for (int i = 0; i < colorsTxt.length; i++) {
//            Log.e("colorsTxt array", colorsTxt[i]);
            int newColor = Color.parseColor(colorsTxt[i]);
            colors.add(newColor);
        }

//        Log.e("In Image editing ", "huhuy");

        int sticker_pos = getIntent().getIntExtra("sticker_position", 0);
        int back_pos = getIntent().getIntExtra("back_position", 0);
        int cat_id = getIntent().getIntExtra("cat id", 0);
        String str = getIntent().getStringExtra("sbg");
//        Log.e("imageedit sbg---------", str);
//        Log.e("Sticker pos---------", String.valueOf(sticker_pos));
//        Log.e("back position---------", String.valueOf(back_pos));
//        Log.e("cat id---------", String.valueOf(cat_id));

//        imageLoader = new ImageLoader(this);
//        imageLoader.DisplayImage(SplashActivity.temp.get(cat_id).background_list.get(back_pos), R.color.transparent, background_img);

        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        imageLoader.init(config);

//        imageLoader.displayImage(SplashActivity.temp.get(cat_id).background_list.get(back_pos),background_img);

//        Glide.with(this)
//                .load(SplashActivity.temp.get(cat_id).background_list.get(back_pos))
//                .override(500,500)
//                .into(background_img);

        Picasso.get()
                .load(SplashActivity.temp.get(cat_id).background_list.get(back_pos))
                .into(background_img);

//        Glide.with(ImageditActivity.this)
//                .load(SplashActivity.temp.get(cat_id).background_list.get(back_pos))
//                .override(500,500)
//                .into(background_img);

        addSpot(getResources().getIdentifier(str, "drawable", getPackageName()));
        for (int i = 0; i < StickerStatic_list.Stickerlist.size(); i++) {

            if (Integer.parseInt(StickerStatic_list.Stickerlist.get(i).getImgId()) == sticker_pos) {
                addSticker(StickerStatic_list.Stickerlist.get(i).getImgUrl());
            }
        }
        Utils.dismissPleaseWait();

        if (slide.isPanelShowing()) {
            flag = true;
        }

        sticker_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        img_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                    Utils.showPleaseWait(ImageditActivity.this);
//                PreviewImage.setImage(getMainFrameBitmap());
                Dialog dialog = new Dialog(ImageditActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                dialog.setContentView(R.layout.img_preview);
                ImageView preview_img = dialog.findViewById(R.id.preview_img);
//                preview_img.
                image = getMainFrameBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.PNG, 100, stream);

                Glide.with(ImageditActivity.this)
                        .load(stream.toByteArray())
                        .asBitmap()
                        .error(R.drawable.back_1)
                        .into(preview_img);
                dialog.show();
//                    Utils.dismissPleaseWait();

//                Log.e("image preview click ", String.valueOf(finalEditedBitmapImage));
//                Intent previewintent = new Intent(ImageditActivity.this, PreviewActivity.class);
//                  startActivity(previewintent);
            }
        });

        linear_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        openGallery();
                    } else if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                MY_REQUEST_CODE);
                    }
                } else {
                    openGallery();
                }
            }
        });

        img_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showPleaseWait(ImageditActivity.this);
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        create_Save_Image();

                    } else if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                MY_REQUEST_CODE);
                    }
                } else {
                    create_Save_Image();
                }
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        iv_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fontname = new ArrayList<>();
        listAssetFiles("fonts");


        linear_wallpaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                slide.displayPanel();
                onSlideClick();
//                onSlideViewButtonClick();
                setBackground();
            }
        });
        linear_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTextSticker();
            }
        });
        linear_sticker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                slide.displayPanel();
//                onSlideViewButtonClick();
                onSlideClick();
                setStickerList();
            }
        });

        sticker_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeSticker();
                linear_sticker_select.setVisibility(View.GONE);
                linear_shown.setVisibility(View.VISIBLE);
                img_preview.setVisibility(View.VISIBLE);
            }
        });

        linear_txt_font.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSlideViewButtonClick();
//                slide.displayPanel();

                setfonts();
            }
        });
        linear_txt_color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slide.displayPanel();
                setColors();
            }
        });

        linear_txt_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTextEntityEditing();
            }
        });

        linear_txt_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linear_text_panel.setVisibility(View.GONE);
                linear_shown.setVisibility(View.VISIBLE);
                removeSticker();
            }
        });

        linear_txt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linear_text_panel.setVisibility(View.GONE);
                linear_shown.setVisibility(View.VISIBLE);
            }
        });

        // slide the view from below itself to the current position


        test_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSlideViewButtonClick();
            }
        });
        test_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSlideViewButtonClick();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSlideClick();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSlideClick();
            }
        });


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) switch (requestCode) {
            case 2:

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(
                        selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();


                MainActivity.bmp = BitmapFactory.decodeFile(filePath);
                background_img.setImageBitmap(MainActivity.bmp);
//                Intent intentgallary = new Intent(Image.this, ImageEditingActivity.class);
//                startActivity(intentgallary);
                break;

        }
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, RE_GALLERY);
    }

    public void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    public void onSlideViewButtonClick() {
        if (isUp) {
            myView.setVisibility(View.GONE);
            test_recyclr.setVisibility(View.GONE);
            slideDown(myView);
        } else {
            myView.setVisibility(View.VISIBLE);
            test_recyclr.setVisibility(View.VISIBLE);
            slideUp(myView);
        }
        isUp = !isUp;
    }

    public void onSlideClick() {
        if (isBackUp) {
            backView.setVisibility(View.GONE);
            backCat_recyclr.setVisibility(View.GONE);
            cat_recyclr.setVisibility(View.GONE);
            slideDown(backView);
        } else {
            backView.setVisibility(View.VISIBLE);
            backCat_recyclr.setVisibility(View.VISIBLE);
            cat_recyclr.setVisibility(View.VISIBLE);
            slideUp(backView);
        }
        isBackUp = !isBackUp;
    }

    private void setColors() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        back_recyclr.setLayoutManager(gridLayoutManager);
        final ColorAdapter colorAdapter = new ColorAdapter(getApplication(), colors, new CustomCatItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
//                Log.e("font position", String.valueOf(position));
                TextEntity textEntity = currentTextEntity();
                if (textEntity != null) {
                    TextLayer textLayer = textEntity.getLayer();
                    textLayer.getFont().setColor(colors.get(position));
                    textEntity.updateEntity();
                    motionView.invalidate();
                }
            }
        });
        back_recyclr.setAdapter(colorAdapter);
    }

    private void setfonts() {
//        for (int i = 0; i < fontname.size(); i++) {
//            Log.e("font name test ", fontname.get(i));
//        }
//        Log.e("Font test ", "in setfont");
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        test_recyclr.setLayoutManager(gridLayoutManager);
        final FontAdapter fontAdpter = new FontAdapter(getApplication(), fontname, new CustomCatItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
//                font.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/" + fontname.get(position)));
//                Log.e("font position", String.valueOf(position));
                TextEntity textEntity = currentTextEntity();
                Font font = new Font();
                if (textEntity != null) {
                    TextLayer textLayer = textEntity.getLayer();
                    font.setColor(textLayer.getFont().getColor());
                    font.setSize(textLayer.getFont().getSize());
                    font.setTypeface(String.valueOf(position));
                    textLayer.setFont(font);
                    textEntity.updateEntity();
                    motionView.invalidate();
//                    textLayer.getFont().setTypeface(String.valueOf(position));
                }
//                font.setTypeface(String.valueOf(position));
//                textLayer.setFont(font);
//                slide.hide();
            }
        });
        test_recyclr.setAdapter(fontAdpter);
    }

    @Override
    public void textChanged(@NonNull String text) {
        TextEntity textEntity = currentTextEntity();
        if (textEntity != null) {
            TextLayer textLayer = textEntity.getLayer();
            if (!text.equals(textLayer.getText())) {
                textLayer.setText(text);
                textEntity.updateEntity();
                motionView.invalidate();
            }
        }
    }

    private void listAssetFiles(String path) {
//        Log.e("font test", "list asset files");
        final AssetManager assetManager = getAssets();
        try {

            String[] filelist = assetManager.list("fonts");
            if (filelist == null) {
            } else {
                for (int i = 0; i < filelist.length; i++) {
                    fontname.add(filelist[i]);
//                    Log.e("fonts", fontname.get(i));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        Log.e("font test ", String.valueOf(fontname.size()));
//        for (int i = 0; i < fontname.size(); i++) {
//            Log.e("font name test", fontname.get(i));
//        }
    }


    protected void addTextSticker() {
        TextLayer textLayer = createTextLayer();
        TextEntity textEntity = new TextEntity(textLayer, motionView.getWidth(),
                motionView.getHeight(), fontProvider);
        motionView.addEntityAndPosition(textEntity);

        // move text sticker up so that its not hidden under keyboard
        PointF center = textEntity.absoluteCenter();
        center.y = center.y * 0.5F;
        textEntity.moveCenterTo(center);

        // redraw
        motionView.invalidate();

        startTextEntityEditing();
    }

    private TextLayer createTextLayer() {
        textLayer = new TextLayer();
        Font font = new Font();

        font.setColor(TextLayer.Limits.INITIAL_FONT_COLOR);
        font.setSize(TextLayer.Limits.INITIAL_FONT_SIZE);
        font.setTypeface(fontProvider.getDefaultFontName());

        textLayer.setFont(font);

        if (BuildConfig.DEBUG) {
            textLayer.setText("Insta Highlight.");
        }

        return textLayer;
    }

    private void startTextEntityEditing() {
        TextEntity textEntity = currentTextEntity();
        if (textEntity != null) {
            TextEditorDialogFragment fragment = TextEditorDialogFragment.getInstance(textEntity.getLayer().getText());
            fragment.show(getFragmentManager(), TextEditorDialogFragment.class.getName());
        }
    }

    @Nullable
    private TextEntity currentTextEntity() {
        if (motionView != null && motionView.getSelectedEntity() instanceof TextEntity) {
            return ((TextEntity) motionView.getSelectedEntity());
        } else {
            return null;
        }
    }

    public void setStickerList() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        backCat_recyclr.setLayoutManager(gridLayoutManager);
        final StickerAdapter stickerAdpter = new StickerAdapter(getApplication(), 0, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position, int index) {
//                Log.e("position ", String.valueOf(position));
                sticker_pur(index, position);
            }
        });
        backCat_recyclr.setAdapter(stickerAdpter);

        GridLayoutManager gridManager = new GridLayoutManager(ImageditActivity.this, 1);
        gridManager.setOrientation(GridLayoutManager.HORIZONTAL);
        cat_recyclr.setLayoutManager(gridManager);
        final StickerCatAdapter stickerCatAdapter = new StickerCatAdapter(getApplication(), new CustomCatItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
                gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                backCat_recyclr.setLayoutManager(gridLayoutManager);
                final StickerAdapter stickerAdpter = new StickerAdapter(getApplication(), position, new CustomItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position, int index) {
//                Log.e("position ", String.valueOf(position));
                        sticker_pur(index, position);
                    }
                });
                backCat_recyclr.setAdapter(stickerAdpter);
            }
        });
        cat_recyclr.setAdapter(stickerCatAdapter);
//        cat_recyclr.setAdapter(new StickerCatAdapter(ImageditActivity.this, background_img, backCat_recyclr));
    }


    public void setBackground() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(ImageditActivity.this, 2);
        gridLayoutManager.setOrientation(GridLayoutManager.HORIZONTAL);
        backCat_recyclr.setLayoutManager(gridLayoutManager);
        final BackAdapter backAdapter = new BackAdapter(ImageditActivity.this, 0, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position, int index) {

                back_pur(index, position);
            }
        });
        backCat_recyclr.setAdapter(backAdapter);
//        backCat_recyclr.setAdapter(new BackAdapter(ImageditActivity.this, background_img,0));

        GridLayoutManager gridManager = new GridLayoutManager(ImageditActivity.this, 1);
        gridManager.setOrientation(GridLayoutManager.HORIZONTAL);
        cat_recyclr.setLayoutManager(gridManager);
        final BackCatAdapter backCatAdapter = new BackCatAdapter(getApplication(), new CustomCatItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {

//                Log.e("back cat position is ", String.valueOf(position));

                GridLayoutManager gridLayoutManager = new GridLayoutManager(ImageditActivity.this, 2);
                gridLayoutManager.setOrientation(GridLayoutManager.HORIZONTAL);
                backCat_recyclr.setLayoutManager(gridLayoutManager);
                final BackAdapter backAdapter = new BackAdapter(ImageditActivity.this, position, new CustomItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position, int index) {

                        back_pur(index, position);

                    }
                });
                backCat_recyclr.setAdapter(backAdapter);

            }
        });
        cat_recyclr.setAdapter(backCatAdapter);

    }

    public void sticker_pur(int index, int position) {
        if (Integer.parseInt(StickerCatStatic_list.StickerCatlist.get(index).getCatPrem()) == 1 &&
                !appPref.getString(AppPref.Purchase).equals("true")) {
            dialog_purchse();
        } else {
            if (flag) {
                flag = false;
                addadapterSticker(SplashActivity.sticker_temp.get(index).sticker_list.get(position));
            } else {
                removeSticker();
                addadapterSticker(SplashActivity.sticker_temp.get(index).sticker_list.get(position));
            }
        }
    }

    public void back_pur(int index, int position) {
        if (Integer.parseInt(BackCatStatic_list.BackCatlist.get(index).getCatPrem()) == 1 &&
                !appPref.getString(AppPref.Purchase).equals("true")) {
            dialog_purchse();
        } else {
            Utils.showPleaseWait(ImageditActivity.this);

            Glide.with(ImageditActivity.this)
                    .load(SplashActivity.temp.get(index).background_list.get(position))
                    .override(500, 500).into(background_img);

            Utils.dismissPleaseWait();
        }
    }

    public void dialog_purchse() {
        Utils.dismissPleaseWait();
        pur_dialog = new Dialog(ImageditActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        pur_dialog.setContentView(R.layout.purchase);
        Button btn = pur_dialog.findViewById(R.id.purchase);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSlideClick();
                try {
                    mHelper.launchPurchaseFlow(ImageditActivity.this, ITEM_SKU, 10001,
                            mPurchaseFinishedListener, "mypurchasetoken");
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
//                            appPref.setString(AppPref.Purchase,"true");
//                            Log.e("shared preference ",appPref.getString(AppPref.Purchase));
                pur_dialog.dismiss();
            }
        });
        pur_dialog.show();
    }


    private void noSpot(final int spot) {
        motionView.post(new Runnable() {
            @Override
            public void run() {
                Layer layer = new Layer();
                Bitmap pica = BitmapFactory.decodeResource(getResources(), spot);

                ImageEntity entity = new ImageEntity(layer, pica, 1, 1);

                motionView.nospotEntityAndPosition(entity);
//                motionView.release();

            }
        });
    }


    private void addSpot(final int spot) {
        motionView.post(new Runnable() {
            @Override
            public void run() {
                Layer layer = new Layer();
                Bitmap pica = BitmapFactory.decodeResource(getResources(), spot);

                ImageEntity entity = new ImageEntity(layer, pica, motionView.getWidth(), motionView.getHeight());

                motionView.spotaddEntityAndPosition(entity);
            }
        });
    }

    private void removeSticker() {
        motionView.deletedSelectedEntity();
    }

    private void addadapterSticker(final String link) {
        motionView.post(new Runnable() {
            @Override
            public void run() {
//                Bitmap pica = BitmapFactory.decodeResource(getResources(), stickerResId);

                try {


                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

                    StrictMode.setThreadPolicy(policy);
                    Layer layer = new Layer();

                    URL url = new URL(link);
                    Bitmap pica = BitmapFactory.decodeStream(url.openConnection().getInputStream());

                    ImageEntity entity = new ImageEntity(layer, pica, motionView.getWidth(), motionView.getHeight());

                    motionView.addEntityAndPosition(entity);

                } catch (IOException e) {
                    System.out.println(e);
                }


//                motionViewCallback.onStickerEntitySelected(entity);
            }
        });
    }

    private void addSticker(final String link) {
        motionView.post(new Runnable() {
            @Override
            public void run() {
//                Bitmap pica = BitmapFactory.decodeResource(getResources(), stickerResId);

                try {


                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

                    StrictMode.setThreadPolicy(policy);
                    Layer layer = new Layer();

                    URL url = new URL(link);
                    Bitmap pica = BitmapFactory.decodeStream(url.openConnection().getInputStream());

                    ImageEntity entity = new ImageEntity(layer, pica, motionView.getWidth(), motionView.getHeight());

                    motionView.addEntityAndPosition(entity);
                    motionView.removeBorder(entity);
                    linear_sticker_select.setVisibility(View.GONE);
                    linear_shown.setVisibility(View.VISIBLE);
                    img_preview.setVisibility(View.VISIBLE);

                } catch (IOException e) {
                    System.out.println(e);
                }


//                motionViewCallback.onStickerEntitySelected(entity);
            }
        });
    }

    private void create_Save_Image() {

        selectedEntity = motionView.getSelectedEntity();
        motionView.removeBorder(selectedEntity);
        PreviewImage.setImage(getMainFrameBitmap());
//        Log.e("TAG", "saveImageInCache is called");
        finalEditedBitmapImage = getMainFrameBitmap();
        saveImage(finalEditedBitmapImage);

    }


    private Bitmap getMainFrameBitmap() {
        main_frame.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(main_frame.getDrawingCache());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            bitmap.setConfig(Bitmap.Config.ARGB_8888);
        }
        main_frame.setDrawingCacheEnabled(false);
        Bitmap bmp = bitmap;
        int imgHeight = bmp.getHeight();
        int imgWidth = bmp.getWidth();
        int smallX = 0, largeX = imgWidth, smallY = 0, largeY = imgHeight;
        int left = imgWidth, right = imgWidth, top = imgHeight, bottom = imgHeight;
        for (int i = 0; i < imgWidth; i++) {
            for (int j = 0; j < imgHeight; j++) {
                if (bmp.getPixel(i, j) != Color.TRANSPARENT) {
                    if ((i - smallX) < left) {
                        left = (i - smallX);
                    }
                    if ((largeX - i) < right) {
                        right = (largeX - i);
                    }
                    if ((j - smallY) < top) {
                        top = (j - smallY);
                    }
                    if ((largeY - j) < bottom) {
                        bottom = (largeY - j);
                    }
                }
            }
        }
//        Log.d("Trimed bitmap", "left:" + left + " right:" + right + " top:" + top + " bottom:" + bottom);
        bmp = Bitmap.createBitmap(bmp, left, top, imgWidth - left - right, imgHeight - top - bottom);
        return bmp;
    }

    private void saveImage(Bitmap bitmap2) {
        isAlreadySave = true;
//        Log.v("TAG", "saveImageInCache is called");
        Bitmap bitmap;
        OutputStream output;
        // Retrieve the image from the res folder
        bitmap = bitmap2;
        // Create a new folder in SD Card
        dir = new File(Environment.getExternalStorageDirectory() + "/Story Highlight");
        if (!dir.exists()) {
            dir.mkdir();
        }
        // Create a name for the saved image
        String ts = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String FileName = ts + ".jpeg";
        File file = new File(dir, FileName);
        file.renameTo(file);
        final String _uri = Environment.getExternalStorageDirectory() + "/Story Highlight/" + FileName;
        //for share image
        String _uri2 = Environment.getExternalStorageDirectory() + "/Story Highlight/" + FileName;
        _url = _uri2;//used in share image
//        Log.e("cache uri=", _uri);
        try {
            output = new FileOutputStream(file);
            // Compress into png format image from 0% - 100%
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
            output.flush();
            output.close();
            //finish();
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(_uri))));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "Downloaded", Toast.LENGTH_LONG).show();
                Utils.dismissPleaseWait();
                Intent saveintent = new Intent(ImageditActivity.this, SaveActivity.class);
                saveintent.putExtra("save_img", _uri);
                startActivity(saveintent);
            }
        }, 3000);

    }
/*

    private void selector_call_api() {
        RequestQueue requestQueue = Volley.newRequestQueue(ImageditActivity.this);
        //requestQueue.cancelAll(true);
        StringRequest stringRequest = new StringRequest(selector_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                catData = gson.fromJson(response, CatData.class);
                GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
                gridLayoutManager.setOrientation(GridLayoutManager.HORIZONTAL);
                selector_recyclr.setLayoutManager(gridLayoutManager);
                selector_recyclr.setAdapter(new cat_adapter(ImageditActivity.this, catData, back_recyclr, img_bg));
                for (int i = 0; i < catData.getCatList().size(); i++) {
                    Log.e("cat data ", catData.getCatList().get(i).getCatId());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ImageditActivity.this, "" + error, Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(stringRequest);
    }

*/

    @Override
    public void onBackPressed() {
        if (slide.isPanelShowing()) {
            slide.hide();
            return;
        } else if (isUp) {
            onSlideViewButtonClick();
        } else if (isBackUp) {
            onSlideClick();
        } else if (linear_text_panel.getVisibility() == View.VISIBLE) {
            linear_text_panel.setVisibility(View.GONE);
            linear_shown.setVisibility(View.VISIBLE);
        } else if (linear_sticker_select.getVisibility() == View.VISIBLE) {
            linear_sticker_select.setVisibility(View.GONE);
            linear_shown.setVisibility(View.VISIBLE);
        } else {
            super.onBackPressed();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case 5:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //openGallery();
                } else {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                MY_REQUEST_CODE);
                    }
                }
                break;
        }
    }


    //******purchase methods


    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        @SuppressLint("LongLogTag")
        public void onIabPurchaseFinished(IabResult result,
                                          Purchase purchase) {
            if (result.isFailure()) {
                // Handle error
//                Log.e("IN APP PUR OnIabPurchaseFinishedListener", "Errorr" + result.getMessage());
                Toast.makeText(ImageditActivity.this, "Purchase Failed Try again please", Toast.LENGTH_LONG).show();
                return;


            } else if (purchase.getSku().equals(ITEM_SKU)) {
                consumeItem();

            }

        }
    };


    public void consumeItem() {
        try {
            mHelper.queryInventoryAsync(mReceivedInventoryListener);
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
        }
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {


            if (result.isFailure()) {
                Toast.makeText(ImageditActivity.this, "Purchase Failed Try again please", Toast.LENGTH_LONG).show();
                // Handle failure
            } else {
                try {
                    mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                            mConsumeFinishedListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase,
                                              IabResult result) {

                    if (result.isSuccess()) {
                        appPref.setString(AppPref.Purchase, "true");
//                        Log.e("shared preference ", appPref.getString(AppPref.Purchase));

                    } else {
                        // handle error
                        Toast.makeText(ImageditActivity.this, "Purchase Failed Try again please", Toast.LENGTH_LONG).show();
                    }
                }
            };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) {
            try {
                mHelper.dispose();
            } catch (IabHelper.IabAsyncInProgressException e) {
                e.printStackTrace();
            }
        }
        mHelper = null;
    }


}
