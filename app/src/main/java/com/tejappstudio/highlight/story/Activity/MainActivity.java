package com.tejappstudio.highlight.story.Activity;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jackandphantom.circularimageview.CircleImage;
import com.tejappstudio.highlight.story.Model.FrameModel;
import com.tejappstudio.highlight.story.Model.SpotModel;
import com.tejappstudio.highlight.story.R;
import com.tejappstudio.highlight.story.adapter.CustomClickListener;
import com.tejappstudio.highlight.story.adapter.adapter;
import com.tejappstudio.highlight.story.app.Config;
import com.tejappstudio.highlight.story.helper.Background.BackStatic_list;
import com.tejappstudio.highlight.story.helper.Background.DataBack;
import com.tejappstudio.highlight.story.helper.DataStatic;
import com.tejappstudio.highlight.story.helper.Stickers.DataSticker;
import com.tejappstudio.highlight.story.helper.Stickers.StickerStatic_list;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    RecyclerView recycler;
    String url = "http://freeblockchaintutorials.com/tempp/highlights/list.php";
    String sticker_url = "http://freeblockchaintutorials.com/tempp/highlights/stick_list.php";
    CircleImage img;
    ArrayList<FrameModel> sticker;
    ArrayList<SpotModel> spot;
    ArrayList<Integer> thumb;
    Toolbar toolbar_main;
    String TAG = "INtersstialll AD";
//    InterstitialAd interstitialAd;
    Ad adfacebook;
    DataBack data;
    DataSticker dataSticker;
    DataStatic dataStatic;
    public static Bitmap bmp;
    private BroadcastReceiver mRegistrationBroadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        toolbar_main = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar_main);

//        interstitialAd = new InterstitialAd(MainActivity.this, "319129505471336_319130505471236");

        recycler = findViewById(R.id.recycler);
        thumb = new ArrayList<>();

        load_page();

//        call_api();
//        addSticker();
//        addspot();
//        fill_sticker();
//        addthumbnail();
//        addallsticker();
//        addstickers();
//        addbackstickers();

//        Log.e("back img size ", String.valueOf(DataStatic.backList.size()));
//        for (int i = 0;i<DataStatic.backList.size();i++){
//            Log.e("back img ",DataStatic.backList.get(i).getImgUrl());
//        }
//
//        Log.e("sticker img size ", String.valueOf(DataStaticSticker.stickList.size()));
//        for (int i = 0;i<DataStaticSticker.stickList.size();i++){
//            Log.e("sticker img ",DataStaticSticker.stickList.get(i).getImgUrl());
//        }




//        Log.e("test arr", String.valueOf(Arrthumb.getGetThumb().get(5).getSticker()));

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
//                    Log.e("message", message);

                }
            }




        };
    }

    public void load_page(){
//        Log.e("before adap","hooooooooooo");
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(gridLayoutManager);
//        Log.e("mid adap","huuuuuuuuuuuiiiiiii");
        recycler.setAdapter(new adapter(MainActivity.this ,recycler, BackStatic_list.Backlist, StickerStatic_list.Stickerlist, new CustomClickListener() {

            public void onItemClick(View v, final int position, int pos1, int pos2, final String str, int pur) {

//                Log.e("maiin sbg---------", str);

/*


                if (Utils.isConnected(MainActivity.this)) {
                    Utils.showPleaseWait(MainActivity.this);
                    if (SplashActivity.count == 1 || SplashActivity.count == 3 || SplashActivity.count == 5 || SplashActivity.count >= 7) {

                        interstitialAd.loadAd();


                    } else {
                        Utils.dismissPleaseWait();
                        Intent imageintent = new Intent(MainActivity.this, ImageditActivity.class);
                        imageintent.putExtra("position", position);
                        imageintent.putExtra("sbg", str);
                        startActivity(imageintent);
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Please check your connection", Toast.LENGTH_LONG).show();
                }
                interstitialAd.setAdListener(new InterstitialAdListener() {
                    @Override
                    public void onInterstitialDisplayed(Ad ad) {
                        // Interstitial ad displayed callback

                        Log.e(TAG, "Interstitial ad displayed.");
                    }

                    @Override
                    public void onInterstitialDismissed(Ad ad) {
                        // Interstitial dismissed callback
                        Utils.dismissPleaseWait();
                        Intent intent = new Intent(MainActivity.this, ImageditActivity.class);
                        intent.putExtra("position", position);
                        intent.putExtra("sbg", str);
                        startActivity(intent);
                        Log.e(TAG, "Interstitial ad dismissed.");
                    }

                    @Override
                    public void onError(Ad ad, AdError adError) {
                        // Ad error callback
                        Utils.dismissPleaseWait();
                        Intent intent = new Intent(MainActivity.this, ImageditActivity.class);
                        intent.putExtra("position", position);
                        intent.putExtra("sbg", str);
                        startActivity(intent);
                        Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                        // Interstitial ad is loaded and ready to be displayed
                        Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                        // Show the ad
                        interstitialAd.show();
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
                        // Ad clicked callback
                        Utils.dismissPleaseWait();
                        Intent intent = new Intent(MainActivity.this, ImageditActivity.class);
                        intent.putExtra("position", position);
                        intent.putExtra("sbg", str);
                        startActivity(intent);
                        Log.d(TAG, "Interstitial ad clicked!");
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
                        // Ad impression logged callback
                        Log.d(TAG, "Interstitial ad impression logged!");
                    }
                });
*/

            }
//
        }));
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

//        Log.e(TAG, "Firebase reg id: " + regId);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.share_app) {
            share();
            return true;
        }
        if (id == R.id.more_app) {
            moreapps();
            return true;
        }
        if (id == R.id.rate_us) {
            rateus();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    private void addbackstickers() {
        for (int i = 1; i <= 6; i++) {
            BackSticker bs = new BackSticker();
            bs.setStick(getResources().getIdentifier("r" + i, "drawable", getPackageName()));
            ListBackSticker.sticker.add(bs);
        }
        for (int i = 1; i <= 103; i++) {
            BackSticker bs = new BackSticker();
            bs.setStick(getResources().getIdentifier("t" + i, "drawable", getPackageName()));
            ListBackSticker.sticker.add(bs);
        }
    }


    private void addstickers() {
        for (int i = 1; i <= 6; i++) {
            Stickers s = new Stickers();
            s.setSti(getResources().getIdentifier("sbg_" + i, "drawable", getPackageName()));
            ListSticker.sticker.add(s);
        }
        for (int i = 1; i <= 103; i++) {
            Stickers s = new Stickers();
            s.setSti(getResources().getIdentifier("a" + i, "drawable", getPackageName()));
            ListSticker.sticker.add(s);
        }
    }

//    private void addthumbnail() {
//        for (int i = 1; i <= 38; i++) {
//            thumb.add(getResources().getIdentifier("mian_" + i, "drawable", getPackageName()));
//        }
//    }


    private void addallsticker() {
        for (int i = 1; i <= 38; i++) {
            Thumbnail t = new Thumbnail();
            t.setBack(getResources().getIdentifier("back_" + i, "drawable", getPackageName()));
            t.setSticker(getResources().getIdentifier("a" + i, "drawable", getPackageName()));
            if (i == 9) {
                t.setSpot(getResources().getIdentifier("sbg_1", "drawable", getPackageName()));
            } else if (i == 21) {
                t.setSpot(getResources().getIdentifier("sbg_1", "drawable", getPackageName()));
            } else {
                t.setSpot(getResources().getIdentifier("sbg_2", "drawable", getPackageName()));
            }
            Arrthumb.getThumb.add(t);
        }
    }
*/

    /* public void call_api() {

     *//*StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });*//*

        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        //requestQueue.cancelAll(true);
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "" + error, Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(stringRequest);
    }
*/
    private void share() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
        String sAux = "Insta Highlight\n\n";
        sAux = sAux + "https://play.google.com/store/apps/details?id=" + getPackageName();
        i.putExtra(Intent.EXTRA_TEXT, sAux);
        startActivity(Intent.createChooser(i, "choose one"));
    }

    private void moreapps() {
        String market_uri = "https://play.google.com/store/apps/developer?id=Tej+App+Studio";
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(market_uri));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
                .FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    private void rateus() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    }

    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this).setTitle("Exit App").setMessage("Do you want to exit app?").setPositiveButton("Yes", new ExitApp()).setNegativeButton("No", null).show();

    }

    private class ExitApp implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {

            finish();
        }
    }

    @Override
    protected void onResume() {
//        Log.e("call on Resume","heeey");
        load_page();
        super.onResume();
    }
}
