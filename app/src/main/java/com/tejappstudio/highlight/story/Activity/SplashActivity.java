package com.tejappstudio.highlight.story.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tejappstudio.highlight.story.R;
import com.tejappstudio.highlight.story.helper.Background.BackCatStatic_list;
import com.tejappstudio.highlight.story.helper.Background.BackCat_list;
import com.tejappstudio.highlight.story.helper.Background.BackStatic_list;
import com.tejappstudio.highlight.story.helper.Background.Back_list;
import com.tejappstudio.highlight.story.helper.Background.DataBack;
import com.tejappstudio.highlight.story.helper.Background.DataBackCat;
import com.tejappstudio.highlight.story.helper.Images.Image;
import com.tejappstudio.highlight.story.helper.Images.StickerImage;
import com.tejappstudio.highlight.story.helper.Stickers.DataSticker;
import com.tejappstudio.highlight.story.helper.Stickers.DataStickerCat;
import com.tejappstudio.highlight.story.helper.Stickers.StickerCatStatic_list;
import com.tejappstudio.highlight.story.helper.Stickers.StickerCat_list;
import com.tejappstudio.highlight.story.helper.Stickers.StickerStatic_list;
import com.tejappstudio.highlight.story.helper.Stickers.Sticker_list;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS = 1;
    //    public static int count = 0;
    int count = 1;
    DataBack data;
    DataSticker dataSticker;
    public static int ad_count = 0;


    public static ArrayList<Image> temp = new ArrayList<>();
    public static ArrayList<StickerImage> sticker_temp = new ArrayList<>();


    private static final String TAG = MainActivity.class.getSimpleName();


    @SuppressLint("WrongThread")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);


        deleteCache(SplashActivity.this);


        fill_backCat();
        fill_stickerCat();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
//                Log.e("hello ------------ run", "huuuuuuuureeeeeeeeee");
                fill_back();
                fill_sticker();
            }
        }, 1200);


//        MyFirebaseInstanceIDService m = new MyFirebaseInstanceIDService();
//        m.onTokenRefresh();

/*
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if (Build.VERSION.SDK_INT < 23) {
                    statact();
                } else {
                    if (checkAndRequestPermissions()) {
                        statact();
                    }
                }
            }
        }, 5000);
        */

    }


    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    private void fill_back() {
        RequestQueue requestQueue = Volley.newRequestQueue(SplashActivity.this);
        //requestQueue.cancelAll(true);
        StringRequest stringRequest = new StringRequest(getResources().getString(R.string.back_list), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    data = gson.fromJson(response, DataBack.class);

                    for (int i = 0; i < data.getBackList().size(); i++) {

                        for (int j = 0; j < temp.size(); j++) {
                            if (temp.get(j).cat == Integer.parseInt(data.getBackList().get(i).getImgCat()))
                                temp.get(j).background_list.add(getResources().getString(R.string.img) + data.getBackList().get(i).getImgUrl());
                        }

                    }

                    for (int i = 0; i < data.getBackList().size(); i++) {

                        Back_list b = new Back_list();
                        b.setImgId(data.getBackList().get(i).getImgId());
                        b.setImgUrl(getResources().getString(R.string.img) + data.getBackList().get(i).getImgUrl());
                        b.setImgCat(data.getBackList().get(i).getImgCat());
                        BackStatic_list.Backlist.add(b);


                        if (i == data.getBackList().size() - 1) {
                            if (count == 4) {
                                statact();
                            } else {

                                count++;
                            }
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SplashActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                Log.e("Volley Error------", error.toString());
            }
        });
        requestQueue.add(stringRequest);

    }

    private void fill_backCat() {
        RequestQueue requestQueue = Volley.newRequestQueue(SplashActivity.this);
        //requestQueue.cancelAll(true);
        StringRequest stringRequest = new StringRequest(getResources().getString(R.string.backCat_list), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    DataBackCat dataBackCat = gson.fromJson(response, DataBackCat.class);
//                    Log.e("back cat size ", String.valueOf(dataBackCat.getBackCatList().size()));


                    for (int i = 0; i < dataBackCat.getBackCatList().size(); i++) {

                        Image im = new Image();
                        im.cat = Integer.parseInt(dataBackCat.getBackCatList().get(i).getCatId());
                        temp.add(im);

                        BackCat_list b = new BackCat_list();
                        b.setCatId(dataBackCat.getBackCatList().get(i).getCatId());
                        b.setCatImg(dataBackCat.getBackCatList().get(i).getCatImg());
                        b.setCatName(dataBackCat.getBackCatList().get(i).getCatName());
                        b.setCatPrem(dataBackCat.getBackCatList().get(i).getCatPrem());
                        BackCatStatic_list.BackCatlist.add(b);

                        if (i == dataBackCat.getBackCatList().size() - 1) {
                            if (count == 4) {
                                statact();
                            } else {
                                count++;
                            }
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SplashActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                Log.e("Volley Error------", error.toString());
            }
        });
        requestQueue.add(stringRequest);

    }


    private void fill_sticker() {
        RequestQueue requestQueue = Volley.newRequestQueue(SplashActivity.this);
        //requestQueue.cancelAll(true);
        StringRequest stringRequest = new StringRequest(getResources().getString(R.string.stick_list), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    dataSticker = gson.fromJson(response, DataSticker.class);


//                    Log.e("test sticker size", String.valueOf(dataSticker.getStickList().size()));

                    for (int i = 0; i < dataSticker.getStickList().size(); i++) {
                        for (int j = 0; j < sticker_temp.size(); j++) {
                            if (sticker_temp.get(j).cat == Integer.parseInt(dataSticker.getStickList().get(i).getCatId()))
                                sticker_temp.get(j).sticker_list.add(getResources().getString(R.string.sticker) + dataSticker.getStickList().get(i).getImgUrl());
                        }
                    }

                    for (int i = 0; i < dataSticker.getStickList().size(); i++) {

                        Sticker_list s = new Sticker_list();
                        s.setCatId(dataSticker.getStickList().get(i).getCatId());
                        s.setImgId(dataSticker.getStickList().get(i).getImgId());
                        s.setImgUrl(getResources().getString(R.string.sticker) + dataSticker.getStickList().get(i).getImgUrl());
//                        Log.e("test sticker url",dataSticker.getStickList().get(i).getImgUrl());
                        StickerStatic_list.Stickerlist.add(s);

                        if (i == dataSticker.getStickList().size() - 1) {
                            if (count == 4) {
                                statact();
                            } else {
                                count++;
                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
//                    Log.e("IN catch-----", "----------------------------------");
                }

//                Log.e("back size is ", String.valueOf(sticker_temp.size()));
//                for (int i = 0; i < sticker_temp.size(); i++) {
//                    Log.e("back index is ", String.valueOf(i));
//                    Log.e("back size is ", String.valueOf(sticker_temp.get(i).sticker_list.size()));
//                    for (int j = 0; j < sticker_temp.get(i).sticker_list.size(); j++) {
//                        Log.e("back url is ", sticker_temp.get(i).sticker_list.get(j));
//                    }
//                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                statact();
                Toast.makeText(SplashActivity.this, "" + error, Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(stringRequest);

    }


    private void fill_stickerCat() {
        RequestQueue requestQueue = Volley.newRequestQueue(SplashActivity.this);
        //requestQueue.cancelAll(true);
        StringRequest stringRequest = new StringRequest(getResources().getString(R.string.stickCat_list), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    DataStickerCat dataStickerCat = gson.fromJson(response, DataStickerCat.class);


//                    Log.e("test sticker size", String.valueOf(dataStickerCat.getStickCatList().size()));
                    for (int i = 0; i < dataStickerCat.getStickCatList().size(); i++) {

                        StickerImage sti = new StickerImage();
                        sti.cat = Integer.parseInt(dataStickerCat.getStickCatList().get(i).getCatId());
                        sticker_temp.add(sti);

                        StickerCat_list s = new StickerCat_list();
                        s.setCatId(dataStickerCat.getStickCatList().get(i).getCatId());
                        s.setCatImg(dataStickerCat.getStickCatList().get(i).getCatImg());
                        s.setCatName(dataStickerCat.getStickCatList().get(i).getCatName());
                        s.setCatPrem(dataStickerCat.getStickCatList().get(i).getCatPrem());
                        StickerCatStatic_list.StickerCatlist.add(s);

                        if (i == dataStickerCat.getStickCatList().size() - 1) {
                            if (count == 4) {
                                statact();
                            } else {

                                count++;
                            }
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SplashActivity.this, "" + error, Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(stringRequest);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCOUNTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

//                    statact();
                } else {
                    //You did not accept the request can not use the functionality.
                    Toast.makeText(SplashActivity.this, "you have not permission", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private boolean checkAndRequestPermissions() {
        int permissionCAMERA = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);


        int storagePermission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_PERMISSIONS_REQUEST_ACCOUNTS);
            return false;
        }

        return true;
    }

    private void statact() {

        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();

    }


}
