package com.tejappstudio.highlight.story.helper;

import android.graphics.Bitmap;

public class PreviewImage {
    public  static Bitmap image;

    public static Bitmap getImage() {
        return image;
    }

    public static void setImage(Bitmap image) {
        PreviewImage.image = image;
    }
}
